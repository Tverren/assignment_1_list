#include "list.h"
#include <memory>

namespace mylib {

  //constructor for Class List
  List::List(std::initializer_list<value_type> list) : _size {
    list.size()}{
    //skal bruke push back til å legge verdiene til listen

      List::push_back(_size);

      /*for(auto new_auto : list){
                      push_back(new_auto.list);
                  }*/
  }

  //push_back
  void List::push_back(int tall) {
      std::shared_ptr<ListItem> new_item = std::make_shared<ListItem>(tall);

      //checks if next pointer to current item is a nullpointer
      std::shared_ptr<ListItem> current_next = _root._next;
      while (current_next != nullptr) {
          current_next = current_next->_next;
      }
      current_next = new_item; // appends new item to the end of the list...making it the nullpointer
  }

  //calls a function method from list.h
  List::size_type
  List::size() const {
      return _size;
  }

  //destructor for Class List
  List::~List(){}

  //constructor for Struct ListItem
  ListItem::ListItem(ListItem::size_type n){
      _value = n;
      _next = nullptr;
  }

  //destructor for Struct ListItem
  ListItem::~ListItem(){}

} // END namespace mylib


