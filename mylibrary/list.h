#ifndef LIST_H
#define LIST_H


#include <initializer_list>
#include <memory>


namespace mylib {

  //item class
  struct ListItem{

      //type
      using size_type = size_t;
      //variable
      int  _value;
      //Info
      size_type size() const;

      //makes a shared pointer of type ListItem
      std::shared_ptr<ListItem> _next;

      //constructors
      ListItem(){_next = nullptr;};
      ListItem(size_type n);
      //ListItem(std::initializer_list<int> lst);
      //ListItem(ListItem && listvalue);
      //destructor
      ~ListItem();

      //
      ListItem& operator=( ListItem&& listvalue) = default;
  };//END item class

  //list class
  class List {
  public:

    //Type
    using size_type  = std::size_t;
    using value_type = ListItem;

    void push_back(int tall);

    //returns size of list
    size_type size() const;


    List();//a default constructor that shall not get any sort of values.

    ~List();//a destructor for that constructor

    //intialize with a list of value_type
    List( std::initializer_list<value_type> list );

  private:
    //Info
    size_type _size;
    ListItem _root;

  }; // END class List
}  // END namespace mylib

#endif // LIST_H
