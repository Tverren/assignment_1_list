
#include <gtest/gtest.h>

#include "../list.h"



TEST(Container_List,Size_functionality) {

  using namespace mylib;

  mylib::List list { 1,2,3,4 };

/*
  list<int>::const_iterator i;
  for(i = list.begin(); i != list.end(); i++)
      std::cout << *i << " ";
  std::cout << std::endl;*/

  EXPECT_EQ(4,list.size());
}
