// mylib
#include "mylibrary/functions.h"
#include "mylibrary/list.h"

// stl
#include <iostream>
#include <stdexcept>

// The one and only "allowed" MACRO -- MACROs are NO NO NO NOOOOOOOOOOOOO !!!! 
// NOT EVEN SLIGHTLY TYPE SAFE - JUST LOOK AT WHAT THIS DOES (... OR DOES NOT)
#define UNUSED(ARG) (void)ARG;

int main(int argc, char** argv) try {
  UNUSED(argc)
  UNUSED(argv)

  //mylib::helloWorld( "... anywho ..." );

  //mylib::List list{1,2,3,4};

  mylib::List item_list = {1,2,3,4,5,6,7,8};


  return 0;
}
catch(const std::exception& e){
  std::cerr << "An exception occurred: " << e.what() << std::endl;
}
catch(...) {
  std::cerr << "Unknown exception thrown!" << std::endl;
}
